from .serializers import (PostSerializer, NotificationSerializer, ActivitySerializer)
import json
from rest_framework.response import Response
from rest_framework import viewsets
from .constants import *
from .models import(Activity, Post, Notification)
from .serializers import (PostSerializer, NotificationSerializer,
        ActivitySerializer)
from .constants import *
from .tasks import (post_creation_notification)


class PostView(viewsets.ModelViewSet):
    """
    View to add ordered items for invoice line items
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def retrieve(self, request, *args, **kwargs):
        post = self.get_object()
        if post.status == INACTIVE:
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(post)
        return Response(status=HTTP_200,
                        data = {'post': serializer.data},
                        content_type='application/json')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(status=ACTIVE)
        if not queryset.exists():
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(queryset, many=True)
        return Response(status=HTTP_200,
                        data = {'post': serializer.data},
                        content_type='application/json')

    def create(self, request, *args, **kwargs):
        data = json.loads(json.dumps(request.data.dict()))
        mandatory_params = ['content', 'posted_by']
        if any(map(lambda m: False if m in data else True ,
                    mandatory_params)):
            params_missing = set(mandatory_params) - set(
                                data)
            return Response(status = HTTP_400,
                    data = {MISSING_IN_REQUEST: ','.join(params_missing)},
                    content_type='application/json')
        serializer_data = {k:data[k] for k in mandatory_params}
        serializer = self.serializer_class(data=serializer_data)
        if serializer.is_valid():
            post = serializer.save()
            post_creation_notification.delay(post.pk)
            return Response(status = HTTP_201,
                data={POST_CREATION_SUCCESS: serializer.data},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={POST_CREATION_FAILURE:serializer.errors},
                content_type='application/json')

    def update(self, request, *args, **kwargs):
        ''' Updates a user post'''
        post = self.get_object()
        data = json.loads(json.dumps(request.data.dict()))
        params = ['content', 'posted_by']
        if not any(map(lambda m: False if m in data else True ,
                    params)):
            return Response(status = HTTP_400,
                    data = {'Illegal Request': NO_PARAMS_SENT},
                    content_type='application/json')
        serializer = self.serializer_class(post, data=serializer_data)
        if serializer.is_valid():
            post = serializer.save()
            return Response(status = HTTP_200,
                data={POST_UPDATION_SUCCESS: serializer.data},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={POST_UPDATION_FAILURE:serializer.errors},
                content_type='application/json')

    '''Soft Deletion Performed'''
    def destroy(self, request, *args, **kwargs):
        post = self.get_object()
        post.delete_post()
        return Response(status = HTTP_204,
                content_type='application/json')



class ActivityView(viewsets.ModelViewSet):
    """
    View to add ordered items for invoice line items
    """
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    def retrieve(self, request, *args, **kwargs):
        activity = self.get_object()
        if activity.status == INACTIVE:
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(activity)
        return Response(status=HTTP_200,
                        data = {'activity': serializer.data},
                        content_type='application/json')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(status=ACTIVE)
        if not queryset.exists():
            return Response(status=HTTP_403,
                        content_type='application/json')
        serializer = self.serializer_class(queryset, many=True)
        return Response(status=HTTP_200,
                        data = {'activity': serializer.data},
                        content_type='application/json')

    def create(self, request, *args, **kwargs):
        data = json.loads(json.dumps(request.data.dict()))
        mandatory_params = ['activity_mode']
        if any(map(lambda m: False if m in data else True ,
                    mandatory_params)):
            params_missing = set(mandatory_params) - set(
                                data)
            return Response(status = HTTP_400,
                    data = {MISSING_IN_REQUEST: ','.join(params_missing)},
                    content_type='application/json')
        serializer_data = {k:data[k] for k in mandatory_params}
        if 'activity_content' in data:
            serializer_data['activity_content'] = data['activity_content']
        serializer = self.serializer_class(data=serializer_data)
        if serializer.is_valid():
            post = serializer.save()
            return Response(status = HTTP_201,
                data={ACTIVITY_CREATION_SUCCESS: serializer.data},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={ACTIVITY_CREATION_FAILURE:serializer.errors},
                content_type='application/json')


    def update(self, request, *args, **kwargs):
        ''' Updates an activity'''
        activity = self.get_object()
        data = json.loads(json.dumps(request.data.dict()))
        mandatory_params = ['post', 'taken_by']
        if any(map(lambda m: False if m in data else True ,
                    mandatory_params)):
            params_missing = set(mandatory_params) - set(
                                data)
            return Response(status = HTTP_400,
                data = {MISSING_IN_REQUEST: ','.join(params_missing)},
                content_type='application/json')
        serializer_data = {k:data[k] for k in mandatory_params}
        if 'activity_content' in data:
            serializer_data['activity_content'] = data['activity_content']
        serializer = self.serializer_class(activity, data=serializer_data,
                context={'taken_by': serializer_data.pop('taken_by'),
                'post': serializer_data.pop('post')})
        if serializer.is_valid():
            activity = serializer.save()
            return Response(status = HTTP_200,
                data={ACTIVITY_UPDATION_SUCCESS: serializer.data},
                content_type='application/json')
        else:
            return Response(status=HTTP_400,
                data={ACTIVITY_UPDATION_FAILURE:serializer.errors},
                content_type='application/json')

    ''' Soft Deletion Performed'''
    def destroy(self, request, *args, **kwargs):
        activity = self.get_object()
        activity.delete_activity()
        return Response(status = HTTP_204,
                content_type='application/json')