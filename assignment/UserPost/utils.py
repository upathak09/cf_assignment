from django.core.exceptions import ValidationError
from .constants import *

def creation_notification(notification_data):
    from .serializers import NotificationSerializer

    serializer = NotificationSerializer(data=notification_data)
    if serializer.is_valid():
        notification = serializer.save()
        #log data here
    else:
        raise ValidationError('Error in Saving Notification Instance')
        #log data here


def get_user_message(activity_user_map, post_id, user_activity,
        action_user_name):
    user_message_map = {}
    for mode, user_set in activity_user_map.iteritems():
        for user in user_set:
            if user.email not in user_message_map:
                user_message_map[user.email] = {'name': None,
                    'email_message': None, 'mode': mode}
            email_str = ACTIIVITY_NOTIFICATION_MESSAGE.format(
                user.name, post_id, ACTIIVITY_NOTIFICATION_MESSAGE_MAP[mode],
                ACTIIVITY_NOTIFICATION_MESSAGE_MAP[user_activity],
                action_user_name)
            user_message_map[user.email]['email_message'] = email_str
    return user_message_map