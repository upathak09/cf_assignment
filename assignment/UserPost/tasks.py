from __future__ import absolute_import
from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from .models import Post, Notification
from .constants import *
from .utils import creation_notification, get_user_message


@shared_task
def post_creation_notification(post_id):
    try:
        p = Post.objects.get(pk=post_id)
    except:
        pass
    user = p.posted_by
    name = user.name
    to_email = user.email
    subject = POST_CREATION_SUBJECT
    email_text = POST_CREATION_EMAIL_TEXT.format(name)
    notification_data = {'related_post': p.pk, 'notif_content': email_text}
    try:
        creation_notification(notification_data)
    except Exception as e:
        #log here
        print str(e)

    #send_email(subject, to_email, email_text)
    return

''' Async task to perform notification sending and multiple inserts;
    Using Bulk_Create and batch sizes of 100 each
'''
@shared_task
def activity_notification(post_id, action_user_id, activity_mode,
        action_user_name):
    all_post = Post.objects.prefetch_related(
        'activity_map', 'activity_map__taken_by')
    try:
        p = all_post.get(pk=post_id)
    except:
        ## log here
        pass
    activity = p.activity_map.all()
    activity_user_map = {}
    for a in activity:
        activity_user_map[a.activity_mode] = set(
        [item for item in a.taken_by.all() if not item.pk == action_user_id])
    user_message_map = get_user_message(activity_user_map, post_id,
        activity_mode, action_user_name)
    print user_message_map, 49
    def_subject, bulk_notification = POST_ACTIVITY_SUBJECT, []

    ## Mail Sending Feature has been commented ##

    ## send email to all the users having taken action on the post ##

    ## create list for Bulk_Update of Notification Model ##
    for email, data_dict in user_message_map.iteritems():
        subject = def_subject.format(data_dict['mode'])
        to_email = email
        email_text = data_dict['email_message']
        notification_data = {'related_post': p,
            'notif_content': email_text,}
        bulk_notification.append(Notification(**notification_data))
        #send_email(subject, to_email, email_text)
    try:
        Notification.objects.bulk_create(bulk_notification, batch_size=100)
    except Exception as e:
        #log here
        print str(e)
    return




def send_email(subject, to_email, email_text, from_email = None):
    to = [to_email]
    msg = EmailMultiAlternatives(subject, email_text, from_email, to)
    try:
        msg.send()
    except:
        pass