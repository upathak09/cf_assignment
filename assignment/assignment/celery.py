from __future__ import absolute_import

import os
from celery import Celery
# from .settings import CELERY_BROKER_URL
# from .settings import BROKER_URL

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'assignment.settings')

app = Celery('assignment',
            backend='amqp://',
            include=['assignment'])
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()